import { ICartState, IProduct } from "@/types/StateProductTypes.interface";
import { mount } from "@vue/test-utils";
import { createRouter, createMemoryHistory } from "vue-router";

import App from "@/App.vue";
import HomeView from "@/views/HomeView.vue";
import FlyersView from "@/views/FlyersView.vue";
import PostersView from "@/views/PostersView.vue";
import BusinessCardsView from "@/views/BusinessCardsView.vue";

import { Commit, createStore } from "vuex";
import {
  businesscardsDefiniton,
  flyersDefinition,
  postersDefiniton,
} from "@/store/product/definitions";

interface RootState {
  cart: ICartState;
  product: IProduct;
}

const store = createStore<RootState>({
  modules: {
    cart: {
      state: {
        products: [],
        productCount: 0,
      },
      mutations: {
        ADD_PRODUCT(state, product) {
          state.products.push(product);
          state.productCount++;
        },
      },
      actions: {
        addProduct({ commit }: { commit: Commit }, product: IProduct) {
          commit("ADD_PRODUCT", product);
        },
      },
      getters: {
        getProductCount: (state: ICartState) => {
          return state.productCount;
        },
      },
    },
    product: {
      state: {
        flyers: {
          ...flyersDefinition,
        },
        businesscards: {
          ...businesscardsDefiniton,
        },
        posters: {
          ...postersDefiniton,
        },
      },
      getters: {
        getObject: () => () => flyersDefinition, // mock getObject
        getOptionsName: () => () => flyersDefinition,
      },
    },
  },
});

describe("router", () => {
  it("renders the Home component when visiting the / route", async () => {
    const router = createRouter({
      history: createMemoryHistory(),
      routes: [{ path: "/", component: HomeView }],
    });

    const wrapper = mount(App, {
      global: {
        plugins: [router, store],
      },
    });

    await router.push("/");
    await router.isReady();
    expect(wrapper.findComponent({ name: "HomeView" }).exists()).toBe(true);
  });

  it("renders the Flyers component when visiting the /products/flyers route", async () => {
    const router = createRouter({
      history: createMemoryHistory(),
      routes: [{ path: "/products/flyers", component: FlyersView }],
    });

    const wrapper = mount(App, {
      global: {
        plugins: [router, store],
      },
    });

    await router.push("/products/flyers");
    await router.isReady();

    expect(wrapper.findComponent({ name: "FlyersView" }).exists()).toBe(true);
  });

  it("renders the Posters component when visiting the /products/posters route", async () => {
    const router = createRouter({
      history: createMemoryHistory(),
      routes: [{ path: "/products/posters", component: PostersView }],
    });

    const wrapper = mount(App, {
      global: {
        plugins: [router, store],
      },
    });

    await router.push("/products/posters");
    await router.isReady();

    expect(wrapper.findComponent({ name: "PostersView" }).exists()).toBe(true);
  });

  it("renders the Business Cards component when visiting the /products/businesscards route", async () => {
    const router = createRouter({
      history: createMemoryHistory(),
      routes: [
        { path: "/products/businesscards", component: BusinessCardsView },
      ],
    });

    const wrapper = mount(App, {
      global: {
        plugins: [router, store],
      },
    });

    await router.push("/products/businesscards");
    await router.isReady();

    expect(wrapper.findComponent({ name: "BusinessCardsView" }).exists()).toBe(
      true
    );
  });
});
