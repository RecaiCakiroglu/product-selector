import { mount } from "@vue/test-utils";
import CartItem from "@/components/Cart/CartItem.vue";
import { ICartState, ICartProduct } from "@/types/StateProductTypes.interface";
import { PropertyElement } from "@/types/ProductTypes.interface";

import { Commit, createStore } from "vuex";

describe("CartItem.vue", () => {
  it("should call deleteProduct action when remove button is clicked", async () => {
    const property: PropertyElement = {
      slug: "95 g/m² Neon magenta paper",
      title: "95 g/m² Neon magenta paper",
      locked: false,
      options: [],
    };
    const product: ICartProduct = {
      clean_cut: property,
      copies: property,
      custom_shape: property,
      customer_packing_option: property,
      delivery: property,
      finish: property,
      material: property,
      packaging_extras: property,
      printingmethod: property,
      printtype: property,
      sheet_size: property,
      size: property,
      id: "1",
      type: "flyers",
    };

    const store = createStore<ICartState>({
      state: {
        products: [product],
        productCount: 1,
      },
      mutations: {
        DELETE_PRODUCT(state: ICartState, productId: number) {
          const productNumber = String(productId);
          state.products = state.products.filter(
            (product) => product.id != productNumber
          );
          state.productCount--;
        },
        SET_PRODUCTS(state: ICartState, products: ICartProduct[]) {
          state.products = products;
        },
      },
      actions: {
        deleteProduct(
          { commit, state }: { commit: Commit; state: ICartState },
          productId: number
        ) {
          const productNumber = String(productId);

          const updatedProducts = state.products.filter(
            (product) => product.id != productNumber
          );
          commit("SET_PRODUCTS", updatedProducts);
          state.productCount--;
        },
      },
      getters: {
        getProductCount: (state: ICartState) => {
          return state.productCount;
        },
      },
    });

    const wrapper = mount(CartItem, {
      props: { product },
      global: {
        mocks: {
          $store: store,
        },
      },
    });

    await wrapper.find(".remove-item").trigger("click");
    expect(store.state.productCount).toBe(0);
  });
});
