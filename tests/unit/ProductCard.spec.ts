import { ICartState, IProduct } from "@/types/StateProductTypes.interface";
import { shallowMount } from "@vue/test-utils";
import ProductCard from "@/components/Layout/Content/ProductCard.vue";
import { ProductTypes } from "@/types/ProductTypes.interface";
import { Commit, createStore } from "vuex";
import {
  businesscardsDefiniton,
  flyersDefinition,
  postersDefiniton,
} from "@/store/product/definitions";

interface RootState {
  cart: ICartState;
  product: IProduct;
}
const store = createStore<RootState>({
  modules: {
    cart: {
      state: {
        products: [],
        productCount: 0,
      },
      mutations: {
        ADD_PRODUCT(state, product) {
          state.products.push(product);
          state.productCount++;
        },
      },
      actions: {
        addProduct({ commit }: { commit: Commit }, product: IProduct) {
          commit("ADD_PRODUCT", product);
        },
      },
      getters: {
        getProductCount: (state: ICartState) => {
          return state.productCount;
        },
      },
    },
    product: {
      state: {
        flyers: {
          ...flyersDefinition,
        },
        businesscards: {
          ...businesscardsDefiniton,
        },
        posters: {
          ...postersDefiniton,
        },
      },
      getters: {
        getObject: () => () => flyersDefinition,
        getOptionsName: () => () => flyersDefinition,
      },
    },
  },
});

const mockToast = {
  open: jest.fn(),
};

const propertyJson: ProductTypes = {
  sku: "flyers",
  title: "Flyers",
  properties: [
    {
      slug: "size",
      title: "Size",
      locked: false,
      options: [
        {
          slug: "a2",
          name: "A2 - 420 x 594 mm",
          nullable: false,
        },
        {
          slug: "a3",
          name: "A3 - 297 x 420 mm",
          nullable: false,
        },
        {
          slug: "a3_landscape",
          name: "A3 horizontal - 420 x 297 mm",
          nullable: false,
        },
        {
          slug: "a4",
          name: "A4 - 210 x 297 mm",
          nullable: false,
        },
      ],
    },
  ],
  excludes: [
    [
      {
        options: [
          "115gr_gesatineerd_mc",
          "150gr_gesatineerd_mc",
          "200gr_gesatineerd_mc",
          "300gr_gesatineerd_mc",
          "115gr_silk_mc",
          "150gr_silk_mc",
        ],
        property: "material",
      },
      {
        options: ["offset"],
        property: "printingmethod",
      },
    ],
  ],
};
const productType = "flyers";

describe("ProductCard", () => {
  it("renders the correct product image based on the 'productType' prop", () => {
    const wrapper = shallowMount(ProductCard, {
      props: {
        productType,
        propertyJson,
      },
      global: {
        plugins: [store],
        mocks: {
          $toast: mockToast,
        },
      },
    });

    const img = wrapper.find("img");
    expect(img.exists()).toBe(true);
  });

  it("adds the correct product to the store when addToCart is called", () => {
    const wrapper = shallowMount(ProductCard, {
      props: {
        productType,
        propertyJson,
      },
      global: {
        plugins: [store],
        mocks: {
          $toast: mockToast,
        },
      },
    });

    const addToCartButton = wrapper.find(".add-to-cart");
    addToCartButton.trigger("click");
    expect(store.state.cart.productCount).toBe(1);
  });
});
