import { shallowMount } from "@vue/test-utils";
import OptionsCard from "@/components//Layout/Sidebar/OptionsCard.vue";

describe("OptionsCard.vue", () => {
  it("renders size options correctly", () => {
    const sizes = [
      "A2 - 420 x 594 mm",
      "A3 - 297 x 420 mm",
      "A3 horizontal - 420 x 297 mm",
      "A4 - 210 x 297 mm",
      "A4 landscape - 297 x 210 mm",
      "A5 - 148 x 210 mm",
      "A5 landscape - 210 x 148 mm",
      "A6 - 105 x 148 mm",
      "A6 landscape - 148 x 105 mm",
      "A7 - 74 x 105 mm",
      "A7 landscape - 105 x 74 mm",
      "A4 large - 148 x 420 mm",
      "A5 large - 105 x 297 mm",
    ];

    const wrapper = shallowMount(OptionsCard, {
      props: {
        property: {
          slug: "size",
          title: "Size",
          locked: false,
          options: sizes.map((size) => ({
            name: size,
            slug: size,
            nullable: false,
          })),
        },
        productType: "flyers",
      },
    });
    const sizeOptions = wrapper.findAll(".form-check-label");
    expect(sizeOptions).toHaveLength(sizes.length);
  });

  it('emits a "select" event when the user clicks on the radioButton', async () => {
    const materials = [
      "95 g/m² Neon yellow paper",
      "95 g/m² Neon green paper",
      "95 g/m² Neon magenta paper",
      "95 g/m² Neon orange paper",
      "115 g/m² Blueback - water resistant",
      "135 g/m² gloss",
      "135 g/m² matt",
      "170 g/m² silk",
      "250 g/m² silk",
      "150 g/m² Citylight",
    ];
    const wrapper = shallowMount(OptionsCard, {
      props: {
        property: {
          slug: "size",
          title: "Size",
          locked: false,
          options: materials.map((material) => ({
            slug: material,
            nullable: false,
          })),
        },
        productType: "posters",
      },
    });
    const card = wrapper.find(".form-check");
    await card.trigger("click");
    expect(wrapper.emitted().select).toBeTruthy();
  });
});
