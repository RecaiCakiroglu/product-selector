import "mdb-vue-ui-kit/css/mdb.min.css";
import { createApp } from "vue";
import ToastPlugin from "vue-toast-notification";

import App from "./App.vue";
import router from "./router";
import store from "./store";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap";
import "vue-toast-notification/dist/theme-bootstrap.css";

createApp(App).use(ToastPlugin).use(store).use(router).mount("#app");
