import { createStore } from "vuex";
import { productModule } from "./product";
import { cartModule } from "./cart";

export default createStore({
  modules: {
    product: productModule,
    cart: cartModule,
  },
});
