import { ICartProduct, ICartState } from "@/types/StateProductTypes.interface";
import { Commit } from "vuex";

export const cartModule = {
  state: {
    products: [],
    productCount: 0,
  },
  mutations: {
    ADD_PRODUCT(state: ICartState, product: ICartProduct) {
      state.products.push(product);
      state.productCount++;
    },
    DELETE_PRODUCT(state: ICartState, productId: number) {
      const productNumber = String(productId);
      state.products = state.products.filter(
        (product) => product.id != productNumber
      );
      state.productCount--;
    },
    SET_PRODUCTS(state: ICartState, products: ICartProduct[]) {
      state.products = products;
    },
  },
  getters: {
    getProductCount: (state: ICartState) => {
      return state.productCount;
    },
  },
  actions: {
    addProduct({ commit }: { commit: Commit }, product: ICartProduct) {
      commit("ADD_PRODUCT", product);
    },
    deleteProduct(
      { commit, state }: { commit: Commit; state: ICartState },
      productId: number
    ) {
      const productNumber = String(productId);

      const updatedProducts = state.products.filter(
        (product) => product.id != productNumber
      );
      commit("SET_PRODUCTS", updatedProducts);
      state.productCount--;
    },
  },
};
