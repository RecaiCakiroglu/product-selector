import { PropertyElement } from "./../../types/ProductTypes.interface";
import { IProduct, IProductState } from "@/types/StateProductTypes.interface";
import {
  businesscardsDefiniton,
  flyersDefinition,
  postersDefiniton,
} from "./definitions";

export const productModule = {
  state: {
    flyers: {
      ...flyersDefinition,
    },
    businesscards: {
      ...businesscardsDefiniton,
    },
    posters: {
      ...postersDefiniton,
    },
  },
  getters: {
    getObject: (state: IProductState) => (objectKey: keyof IProductState) => {
      return state[objectKey];
    },
    getOptionsName:
      (state: IProductState) =>
      (objectKey: keyof IProductState, propertyKey: keyof IProduct) => {
        return state[objectKey][propertyKey]?.options[0].name;
      },
  },
  mutations: {
    UPDATE_PROPERTY(
      state: IProductState,
      payload: {
        objectKey: keyof IProductState;
        propertySlug: keyof IProduct;
        property: PropertyElement;
      }
    ) {
      state[payload.objectKey][payload.propertySlug] = payload.property;
    },
  },
  modules: {},
};
