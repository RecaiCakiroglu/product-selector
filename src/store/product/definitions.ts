import {
  OptionElement,
  PropertyElement,
} from "./../../types/ProductTypes.interface";
import { IProduct } from "@/types/StateProductTypes.interface";

export const mockOption: OptionElement = {
  slug: "N/A",
  name: "N/A",
  nullable: false,
  type: "N/A",
};

export const mockProperty: PropertyElement = {
  slug: "N/A",
  title: "N/A",
  locked: false,
  options: [mockOption],
  type: "N/A",
};

export const postersDefiniton: IProduct = {
  size: mockProperty,
  printtype: mockProperty,
  material: mockProperty,
  finish: mockProperty,
  copies: mockProperty,
  packaging_extras: mockProperty,
  printingmethod: mockProperty,
  sheet_size: mockProperty,
  custom_shape: mockProperty,
  clean_cut: mockProperty,
  customer_packing_option: mockProperty,
  delivery: mockProperty,
};

export const flyersDefinition: IProduct = {
  size: mockProperty,
  printtype: mockProperty,
  material: mockProperty,
  finish: mockProperty,
  spot_finish: mockProperty,
  spot_finish_back: mockProperty,
  copies: mockProperty,
  clean_cut: mockProperty,
  variable_creasing_line: mockProperty,
  perforation: mockProperty,
  personalize: mockProperty,
  die_cut: mockProperty,
  drillholes: mockProperty,
  rounded_corners: mockProperty,
  delivery: mockProperty,
  pallet_delivery: mockProperty,
  sealed: mockProperty,
  bundle: mockProperty,
  standard_bundle: mockProperty,
  cross_bundle: mockProperty,
  printingmethod: mockProperty,
};

export const businesscardsDefiniton: IProduct = {
  fold: mockProperty,
  size: mockProperty,
  printtype: mockProperty,
  delivery: mockProperty,
  material: mockProperty,
  finish: mockProperty,
  spot_finish: mockProperty,
  spot_finish_back: mockProperty,
  copies: mockProperty,
  clean_cut: mockProperty,
  personalize: mockProperty,
  rounded_corners: mockProperty,
  drillholes: mockProperty,
  variable_creasing_line: mockProperty,
  die_cut: mockProperty,
  perforation: mockProperty,
  bundle: mockProperty,
  businesscard_creasing: mockProperty,
  folded: mockProperty,
  printingmethod: mockProperty,
  sheet_size: mockProperty,
};
