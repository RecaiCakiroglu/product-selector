export interface ProductTypes {
  sku: string;
  title: string;
  properties: PropertyElement[];
  excludes: Array<Exclude[]>;
}

export interface Exclude {
  options: Array<number | string>;
  property: string;
}

export interface PropertyElement {
  slug: string;
  title: string;
  locked: boolean;
  options: OptionElement[];
  type?: string;
  optionsInSummary?: string[];
  parentOptions?: ParentOption[];
}

export interface OptionElement {
  slug: number | string;
  name?: null | string;
  nullable: boolean;
  width?: number | string;
  height?: number | string;
  parent?: string;
  eco?: boolean;
  pages?: Page[];
  enrichments?: Enrichment[];
  type?: string;
  description?: string;
}

export interface Enrichment {
  propertySlug: string;
  optionSlug: string;
}

export interface Page {
  numberOfColors: number;
  colorspace: string;
}

export interface ParentOption {
  slug: string;
  name: string;
  nullable: boolean;
  width?: number;
  height?: number;
  eco?: boolean;
  enrichments?: Enrichment[];
}
