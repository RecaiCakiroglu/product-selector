import { PropertyElement } from "@/types/ProductTypes.interface";

export interface IProduct {
  size?: PropertyElement;
  printtype?: PropertyElement;
  material?: PropertyElement;
  finish?: PropertyElement;
  copies?: PropertyElement;
  packaging_extras?: PropertyElement;
  printingmethod?: PropertyElement;
  sheet_size?: PropertyElement;
  custom_shape?: PropertyElement;
  clean_cut?: PropertyElement;
  customer_packing_option?: PropertyElement;
  delivery?: PropertyElement;
  spot_finish?: PropertyElement;
  spot_finish_back?: PropertyElement;
  variable_creasing_line?: PropertyElement;
  perforation?: PropertyElement;
  personalize?: PropertyElement;
  die_cut?: PropertyElement;
  drillholes?: PropertyElement;
  rounded_corners?: PropertyElement;
  pallet_delivery?: PropertyElement;
  sealed?: PropertyElement;
  bundle?: PropertyElement;
  standard_bundle?: PropertyElement;
  cross_bundle?: PropertyElement;
  fold?: PropertyElement;
  businesscard_creasing?: PropertyElement;
  folded?: PropertyElement;
}

export interface ICartProduct extends IProduct {
  id?: string;
  type?: string;
}

export interface IProductState {
  flyers: IProduct;
  businesscards: IProduct;
  posters: IProduct;
}

export interface ICartState {
  products: ICartProduct[];
  productCount: number;
}
